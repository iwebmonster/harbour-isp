<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_tastemngt');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N_SpTksV]W?{:S8PK)n^d7&k`@{I5bS5K4,nH,tAQcKeI+ Bm;I0&Xb=01dXPv*I');
define('SECURE_AUTH_KEY',  '^8TIK5/|B6mug]A|)cU0,[UNy9RL;A3|{:bhMgpLOL7tF{WC[u,XA:i*[hJDn9i&');
define('LOGGED_IN_KEY',    '{B/+ )0Hsh.Q+>s_D$[a2=iP%O_(SK8N;X443H1MT.;8Ao}WQ#9ir^8v:QAAb9<$');
define('NONCE_KEY',        'D5(E9Im-cY%;#,J4R6_J&=XI`qNt3MY^e]4JUkO~KaJ#[iVS!|0elzQ@3Gp+Jq] ');
define('AUTH_SALT',        '/|kz%:=Dt~c-dhK<.)qvr0xpMG4P4_WJF}7BRp[+;.8TYcVbpF$Ugyk?e6w>>!K`');
define('SECURE_AUTH_SALT', '2g( gm0ORkfCoEi cqn-$;`3V8Y)pWoEaC{T{z2]S_m~)-5kQy}.`Ya5^t<g(vk>');
define('LOGGED_IN_SALT',   'pH4Pg3udpB#D9:}UUc=xQL:#EwCg!U%RRhs^K+I2);#uLm.Z55FPlHQXnm/8]AfJ');
define('NONCE_SALT',       ')m!;!jd{dfbPnInI7s;Ugwep?[HL$-2VS+M.b)X]cr,L-g1UF9g_CS,eAAm]V+#2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
