<div class="cart-dropdown">
	<div class="cart-dropdown-inner">
            <?php //echo $woocommerce->cart->get_cart_url(); ?>
		<a class="vamtam-cart-dropdown-link icon theme" href="/hisp/cart/">
			<span class="icon theme"><?php wpv_icon( 'theme-handbag' ) ?></span>
			<span class="products cart-empty">...</span>
		</a>
		<div class="widget woocommerce widget_shopping_cart">
			<div class="widget_shopping_cart_content"></div>
		</div>
	</div>
</div>