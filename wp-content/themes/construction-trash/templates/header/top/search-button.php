<?php
/**
 * Header search button
 *
 * @package wpv
 * @subpackage construction
 */


if ( ! wpv_get_optionb( 'enable-header-search' ) ) return;

?>
    <form method="get" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" >
    	<label class="screen-reader-text" for="s"><?php _e('Search for:', 'woothemes'); ?></label>
        <input type="text" class="field s" name="s" placeholder="<?php esc_attr_e( 'Enter keywords', 'woothemes' ); ?>" />
        <input type="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'woothemes' ); ?>">
    </form>   
<!--<button class="header-search icon wpv-overlay-search-trigger"><?php //wpv_icon( 'search1' ) ?></button>-->