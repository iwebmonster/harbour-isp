$(document).ready(function() {
    
    //Custom Header Style
	$(".normal-logo").attr('style',"max-height:90px;");
	$(".normal-logo").height(90);
	$("#main-menu .menu > .menu-item > a, #main-menu .menu > .menu-item > a:visited").css('color','#f0542d');
	$("header.main-header .first-row, header.main-header.layout-logo-text-menu .first-row > .first-row-wrapper, header.main-header.layout-logo-text-menu .first-row .first-row-left, header.main-header.layout-logo-text-menu .first-row .first-row-right").css('height','126');
	$(".top-nav, .top-nav p").css({'background-color':'#f26633', 'color':'#fff'});
        //$(".header-middle").css({'background-image':'url("http://172.22.104.104/HISP-WP/wp-content/uploads/2015/05/bkng-2015_repeat.jpg")', 'background-repeat':'repeat-x', 'background-position':'0 32px', 'background-color':'#fff'});
	$("#main-menu .menu > .menu-item > a:hover").css('border-bottom','#e61085');
	$(".move-into-slider").css('margin-top','0');
	$(".tp-caption.a-bigtitle-white-thin, .a-bigtitle-white-thin, h2, h2 a, #main-menu .menu .menu-item").css('font-family','Gotham-medium');
	$("header.main-header button.header-search").css('display','none');
	$("h1, h2, h3, h4, h5, h6").css({'font-family':'Gotham-bold','color':'#666'});
        $(".price-wrapper .value-box .value").css({'font-family':'Gotham-bold','font-size':'5em'});
        $(".price-wrapper .price-title,.price-wrapper .price .content-box,.price-wrapper .price .meta-box").css('border-width','0');
	$("header.main-header.layout-logo-text-menu .first-row #header-text > div").css('margin-right','70px');
	$("sup").css('top','-0.2em');
	$(".woocommerce div.product form.cart .variations fieldset strong, .woocommerce div.product form.cart .variations fieldset br").remove();
        
        // unlimited data usage unchecked
        //if ( ! $("#select-your-internet-speed").prop('checked') ) {
        //    $("#select-your-data-usage, .monthly_price").parent().hide();
        //}
        
        //(!$("input[name='html_elements']:checked").val())
        $("#select-your-internet-speed").click(function() {
            var checkvalue = $('input[name=attribute_select-your-internet-speed]:checked').val();
            //alert(checkvalue);
        });
        
        //calling radio button styles
        customRadio("input_6"); 
        customRadio("input_4");
        customRadio("input_3");
        customRadio("input_2");
        customRadio("attribute_pa_voicemail-4-a-month");
        customRadio("attribute_add-4-for-mobile-plan");
        customRadio("attribute_add-14-for-mobile-plan");
        customRadio("attribute_pa_voicemail");
        customRadio("attribute_choose-a-plan");
        customRadio("attribute_add-phone-package"); //local
        customRadio("attribute_add-mobile-package"); // local
        customRadio("attribute_phone-package"); //live
        customRadio("attribute_mobile-package"); //live
        
        //calling checkbox styles
        customCheckbox("input_6.1");
        customCheckbox("input_5.1");
        customCheckbox("input_5.2");
        customCheckbox("input_7.1");
        
        //calling radiobox style
        customRadioBox("attribute_select-your-internet-speed");
        customRadioBox("attribute_select-your-data-usage");
        customRadioBox("attribute_select-your-plan-type");
        
        //add clear on checkout newfields
        $(".page-checkout #my-new-field").addClass('clearfix');
        



// CALLING FANCYBOX
	// Simple image gallery. Uses default settings
        $('.fancybox').fancybox();

	// Different effects

	// Change title type, overlay closing speed
	$(".fancybox-effects-a").fancybox({
            helpers: {
                title : {
                    type : 'outside'
                },
                overlay : {
                    speedOut : 0
                }
            }
	});

	// Disable opening and closing animations, change title type
	$(".fancybox-effects-b").fancybox({
            openEffect  : 'none',
            closeEffect	: 'none',

            helpers : {
                title : {
                    type : 'over'
                }
            }
	});

	// Set custom style, close if clicked, change title type and overlay color
	$(".fancybox-effects-c").fancybox({
            wrapCSS    : 'fancybox-custom',
            closeClick : true,
            openEffect : 'none',
            helpers : {
                title : {
                    type : 'inside'
                },
                overlay : {
                    css : {
                        'background' : 'rgba(238,238,238,0.85)'
                    }
                }
            }
	});

	// Remove padding, set opening and closing animations, close if clicked and disable overlay
	$(".fancybox-effects-d").fancybox({
            padding: 0,
            openEffect : 'elastic',
            openSpeed  : 150,
            closeEffect : 'elastic',
            closeSpeed  : 150,
            closeClick : true,
            helpers : {
                overlay : null
            }
	});


	//Button helper. Disable animations, hide close button, change title type and content
	$('.fancybox-buttons').fancybox({
            openEffect  : 'none',
            closeEffect : 'none',
            prevEffect : 'none',
            nextEffect : 'none',
            closeBtn  : false,
            helpers : {
            title : {
                type : 'inside'
            },
            buttons	: {}
            },
            afterLoad : function() {
                this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
            }
	});

	//Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
	$('.fancybox-thumbs').fancybox({
            prevEffect : 'none',
            nextEffect : 'none',
            closeBtn  : false,
            arrows    : false,
            nextClick : true,
            helpers : {
                thumbs : {
                    width  : 50,
                    height : 50
                }
            }
	});

	//Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
	$('.fancybox-media')
            .attr('rel', 'media-gallery')
            .fancybox({
            openEffect : 'none',
            closeEffect : 'none',
            prevEffect : 'none',
            nextEffect : 'none',
            arrows : false,
            helpers : {
                media : {},
                buttons : {}
            }
	});

	// Open manually
	$("#fancybox-manual-a").click(function() {
            $.fancybox.open('1_b.jpg');
	});

	$("#fancybox-manual-b").click(function() {
            $.fancybox.open({
                href : 'iframe.html',
                type : 'iframe',
                padding : 5
            });
	});

	$("#fancybox-manual-c").click(function() {
            $.fancybox.open([
            {
                href : '1_b.jpg',
                title : 'My title'
            }, {
                href : '2_b.jpg',
                title : '2nd title'
            }, {
                href : '3_b.jpg'
            }
            ], {
                helpers : {
                    thumbs : {
                        width: 75,
                        height: 50
                    }
                }
            });
	});
});

// CUSTOM CHECKBOX AND RADIO BUTTON
    // Radio Button
    function customRadio(radioName){
        var radioButton = $('input[name="' +radioName+ '"]');
        $(radioButton).each(function(){
            $(this).wrap( "<span class='custom-radio'></span>" );
            if($(this).is(':checked')){
                $(this).parent().addClass("selected");
            } else {
                $(this).parent().removeClass("selected");
            }
        });
        $(radioButton).click(function(){
            if($(this).is(':checked')){
                $(this).parent().addClass("selected");
            }
            $(radioButton).not(this).each(function(){
                $(this).parent().removeClass("selected");
            });
        });
    }
    // Boxtype Radio
    function customRadioBox(radioName){
        var radioButton = $('input[name="' +radioName+ '"]');
        $(radioButton).each(function(){
            $(this).wrap( "<span class='custom-radio-box'></span>" );
            if($(this).is(':checked')){
                $(this).parent().addClass("selected");
                $("div.box_select").parent().addClass("pinili");
            } else {
                $(this).parent().removeClass("selected");
                $("div.box_select").parent().removeClass("pinili");
            }
        });
        $(radioButton).click(function(){
            if($(this).is(':checked')){
                $(this).parent().addClass("selected");
                $("div.box_select").parent().addClass("pinili");
            }
            $(radioButton).not(this).each(function(){
                $(this).parent().removeClass("selected");
                $("div.box_select").parent().removeClass("pinili");
            });
        });
        $(radioButton).on('click',function(){
            $('.box_select').removeClass('active');
            $(this).addClass('active');
        });
        
        
    }
    
    // Checkbox
    function customCheckbox(checkboxName){
        var checkBox = $('input[name="' +checkboxName+ '"]');
        $(checkBox).each(function(){
            $(this).wrap( "<span class='custom-checkbox'></span>" );
            if($(this).is(':checked')){
                $(this).parent().addClass("selected");
            }
        });
        $(checkBox).click(function(){
            $(this).parent().toggleClass("selected");
        });
    }