$(document).ready(function() {

	var contract = $('.variation-ContractTerm836918500 .amount, .variation-ContractTerm836910000 .amount, .variation-ContractTerm3610000 .amount, .variation-ContractTerm3618500 .amount').text();
	var voicemail = $('.variation-Voicemail4amonth8369400 .amount, .variation-Voicemail4amonth36400 .amount').text();
	
	var bundles = $('.variation-OptionalExtras83695000 .amount, .variation-OptionalExtras365000 .amount').text();

	var monthly = $('.cart-subtotal td .amount').text();
	
	
	var contract_dec = Number(contract.replace(/[^0-9\.]+/g,""));  //convert to decimal
	var voicemail_dec = Number(voicemail.replace(/[^0-9\.]+/g,""));  //convert to decimal
	var bundles_dec = Number(bundles.replace(/[^0-9\.]+/g,""));  //convert to decimal
	var monthly_dec = Number(monthly.replace(/[^0-9\.]+/g,""));  //convert to decimal
        
	var monthly_total = monthly_dec - (contract_dec + voicemail_dec + bundles_dec);  //grand total minus product add-ons
	var monthly_due = '$'+monthly_total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        
	//alert(bundles_dec);
	if (voicemail_dec==4) {
		$('.product-total .subscription-price .amount, .widget_shopping_cart_content .total .amount').html(monthly_due+' + 4');
	} else {
		$('.product-total .subscription-price .amount, .widget_shopping_cart_content .total .amount').html(monthly_due);
	}

        var sub_total = $('.cart-subtotal').html().replace('/ month', '');
        $('.cart-subtotal').html(sub_total);

        var grand_total = $('.order-total').html().replace('/ month', '');
        $('.order-total').html(grand_total);
        
        var remove_yes_voicemail = $('.variation-Voicemail4amonth8369400 p, .variation-Voicemail4amonth36400 p').html().replace('Yes', '');
        $('.variation-Voicemail4amonth8369400 p, .variation-Voicemail4amonth36400 p').html(remove_yes_voicemail);

});