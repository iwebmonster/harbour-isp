$(document).ready(function() {

    // right side panel
    $(".woocommerce div.product div.images, .woocommerce div.product div.summary").after('<div id="right-side-panel" class="gform_wrapper"><ul class="moved-element gform_fields top_label description_below"></ul></div><div class="clearfix"></div>');
    $(".woocommerce div.product .woocommerce-tabs").before('<div id="second-description"></div>');
        // Jquery for Desktop version
        if ($(window).width() >= 768) {
            //$('.ginput_container').append(" per month");
            // normal plan (bundles, firstpath, opticomm)
            //$("#right-side-panel ul.moved-element").before('<div class="heading-kuno"></div>');
            //$(".gform_wrapper .gform_heading").detach().appendTo('.heading-kuno'); // router heading
            $(".gform_wrapper .gform_heading").hide();
            $(".product_totals").detach().appendTo('#right-side-panel'); // product computation
            $(".gform_body ul li.option_extra").detach().appendTo('#right-side-panel ul.moved-element'); // optional extras
            $(".gform_body ul li.equip_router").detach().appendTo('#right-side-panel ul.moved-element'); // router equipment
            $(".gform_body ul li.equip_phone").detach().appendTo('#right-side-panel ul.moved-element'); // phone equipment
            //$(".variations_button").detach().appendTo('#right-side-panel .product_totals'); //add to cart button
            
            // nbn fibre and fixed wireless
            $(".gform_body ul li.nbn_router").detach().appendTo('#right-side-panel ul.moved-element'); // NBN router
            
            $(".share-btns").detach().appendTo('#right-side-panel'); // social media buttons
            $(".product_meta").detach().appendTo('#right-side-panel'); // SKU
        }
        

    
    
    $(".variations_button .quantity").hide();
    $("body.single-product .single_variation_wrap").hide();
    
    
    $(".not-interested,.yes-add-10-monthly-for-phone-package,.yes-add-10-monthly-for-mobile-package").removeClass('box_select');

    // rearrange short description
    $(".woocommerce div.product").before('<div id="short-description"></div>');
    $(".woocommerce div.product .summary > div[itemprop='description']").detach().appendTo('.page-content div#short-description');
    
    // second description
    $("#tab-description .row").detach().appendTo('#second-description');
    $(".woocommerce div.product .woocommerce-tabs").hide();
    
    // for NBN Fibre
    if ( ($(".up-to121-mbpsonly-49-00 input[type='radio']").prop('checked')) || ($(".up-to255-mbpsonly-59-00 input[type='radio']").prop('checked'))) {
        $(".500gbincluded").hide();
        $(".1000gbadd-10-00").hide();
    }
    if ( ($(".up-to5020-mbpsonly-79-00 input[type='radio']").prop('checked')) || ($(".up-to10040-mbpsonly-89-00 input[type='radio']").prop('checked'))) {
        $(".250gbincluded").hide();
        $(".unlimitedadd-10-00").hide();
    }
   
    // add checked event on data usage
    $('.box_select input[type=radio][name=attribute_select-your-internet-speed]').change(function() {
        console.log("show");
        $(".250gbincluded").hide();
        $(".unlimitedadd-10-00").hide();
        $(".500gbincluded").hide();
        $(".1000gbadd-10-00").hide();
        if ( (this.value == 'up-to121-mbpsonly-49-00') || (this.value == 'up-to255-mbpsonly-59-00') ) {
            $(".250gbincluded").show();
            $(".unlimitedadd-10-00").show();
            
            $('.250gbincluded input[name="attribute_select-your-data-usage"]')[0].checked = true;
            $('.250gbincluded span').addClass('selected');
            $('.unlimitedadd-10-00 span').removeClass('selected');
        }
        else if ( (this.value == 'up-to5020-mbpsonly-79-00') || (this.value == 'up-to10040-mbpsonly-89-00') ) {
            $(".500gbincluded").show();
            $(".1000gbadd-10-00").show();
            
            $('.500gbincluded input[name="attribute_select-your-data-usage"]')[0].checked = true;
            $('.500gbincluded span').addClass('selected');
            $('.1000gbadd-10-00 span').removeClass('selected');
        }
    });
 
    // NBN Satellite options
    if (window.location.href.indexOf("nbn-co-satellite-support-scheme") > -1) {
        $(".single-nbn-co-satellite-support-scheme table.variations td.value").after('<td class="custom-nbn-satellite"></td>');
        peak_satellite_prop(); // loads excess usage
        peak_satellite_switch(); // change excess usage
    }
    
    //add toll number on order button
    $(".woocommerce div.product form.cart .button").after('<div class="call-toll"> or Call 1300 366 169 to signup</div>');
});


function peak_satellite_prop() {
        var peak;
        var txt_msg = "Excess usage price per MB ";

        if ( $('.box_select input[type=radio][value=1gb-3gb-peakoff-peaknss-1only-34-95]').prop('checked') ) {
            peak = "$0.0087";
        } else if ( $('.box_select input[type=radio][value=1gb-4gb-peakoff-peaknss-2only-39-95]').prop('checked') ) {
            peak = "$0.0080";
        } else if ( $('.box_select input[type=radio][value=2gb-6gb-peakoff-peaknss-3only-59-95]').prop('checked') ) {
            peak = "$0.0075";
        } else if ( $('.box_select input[type=radio][value=2gb-8gb-peakoff-peaknss-4only-69-95]').prop('checked') ) {
            peak = "$0.0070";
        } else if ( $('.box_select input[type=radio][value=3gb-9gb-peakoff-peaknss-5only-79-95]').prop('checked') ) {
            peak = "$0.0067";
        } else if ( $('.box_select input[type=radio][value=3gb-12gb-peakoff-peaknss-6only-99-95]').prop('checked') ) {
            peak = "$0.0067";
        } else if ( $('.box_select input[type=radio][value=4gb-12gb-peakoff-peaknss-7only-105-95]').prop('checked') ) {
            peak = "$0.0066";
        } else if ( $('.box_select input[type=radio][value=5gb-15gb-peakoff-peaknss-8only-129-95]').prop('checked') ) {
            peak = "$0.0065";
        } else if ( $('.box_select input[type=radio][value=6gb-18gb-peakoff-peaknss-9only-154-95]').prop('checked') ) {
            peak = "$0.0065";
        } else if ( $('.box_select input[type=radio][value=8gb-24gb-peakoff-peaknss-10only-199-95]').prop('checked') ) {
            peak = "$0.0062";
        }
        $(".custom-nbn-satellite").html('<label>'+txt_msg+'<span>'+peak+'</span></label>');
}

function peak_satellite_switch() {
        var peak;
        var txt_msg = "Excess usage price per MB ";
        $('.box_select input[type=radio][name=attribute_select-your-plan-type]').change(function() {
            switch (this.value) {
                case '1gb-3gb-peakoff-peaknss-1only-34-95': peak = "$0.0087"; break;
                case '1gb-4gb-peakoff-peaknss-2only-39-95': peak = "$0.0080"; break;
                case '2gb-6gb-peakoff-peaknss-3only-59-95': peak = "$0.0075"; break;
                case '2gb-8gb-peakoff-peaknss-4only-69-95': peak = "$0.0070"; break;
                case '3gb-9gb-peakoff-peaknss-5only-79-95': peak = "$0.0067"; break;
                case '3gb-12gb-peakoff-peaknss-6only-99-95': peak = "$0.0067"; break;
                case '4gb-12gb-peakoff-peaknss-7only-105-95': peak = "$0.0066"; break;
                case '5gb-15gb-peakoff-peaknss-8only-129-95': peak = "$0.0065"; break;
                case '6gb-18gb-peakoff-peaknss-9only-154-95': peak = "$0.0065"; break;
                case '8gb-24gb-peakoff-peaknss-10only-199-95': peak = "$0.0062"; break;
            }
            console.log($(".custom-nbn-satellite").html('<label>'+txt_msg+'<span>'+peak+'</span></label>'));
        });
}
