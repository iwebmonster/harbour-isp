<?php
/**
 * Header template
 *
 * @package wpv
 * @subpackage construction
 */
?><!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]> <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]> <html <?php language_attributes(); ?> class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-ie no-js"> <!--<![endif]-->

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- hello World -->
	<title><?php wp_title(); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php wpvge( 'favicon_url' )?>"/>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
        <link href="<?php echo get_template_directory_uri(); ?>/css/custom.css" rel="stylesheet" type="text/css" media="screen" />
        
        
        
<!-- Auto Complete Address -->

        <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.ui.addresspicker.js"></script>
        <script>
            $(document).ready(function(){
                var complete_address = $("#addresspicker_map").val();
                $('#startsearch').click(function(){
                    $('.search-address div.hello_test').show();
                    //$('complete_address').appendTo('.search-address div.hello_test');
                    $('.search-address div.hello_test').text('<p>Text</p>');
                });
                $('#addresspicker_map').click(function(){
                    $('.search-address div.hello_test').hide();
                });
            });
        </script>
        <script>
            $(function() {
                var addresspicker = $( "#addresspicker" ).addresspicker({
                    componentsFilter: 'country:AU'
                });
                var addresspickerMap = $( "#addresspicker_map" ).addresspicker({
                    regionBias: "au",
                    language: "au",
                    updateCallback: showCallback,
                    mapOptions: {
                        zoom: 4,
                        center: new google.maps.LatLng(-25.274398, 133.77513599999997),
                        scrollwheel: false,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    },
                    elements: {
                        map: "#map",
                        lat: "#lat",
                        lng: "#lng",
                        street_number: '#street_number',
                        route: '#route',
                        locality: '#locality',
                        sublocality: '#sublocality',
                        administrative_area_level_3: '#administrative_area_level_3',
                        administrative_area_level_2: '#administrative_area_level_2',
                        administrative_area_level_1: '#administrative_area_level_1',
                        country:  '#country',
                        postal_code: '#postal_code',
                        type:    '#type'
                    }
                });
                
                var gmarker = addresspickerMap.addresspicker( "marker");
                gmarker.setVisible(true);
                addresspickerMap.addresspicker( "updatePosition");

                $('#reverseGeocode').change(function(){
                    $("#addresspicker_map").addresspicker("option", "reverseGeocode", ($(this).val() === 'true'));
                });

                function showCallback(geocodeResult, parsedGeocodeResult){
                    $('#callback_result').text(JSON.stringify(parsedGeocodeResult, null, 4));
                }
                // Update zoom field
                var map = $("#addresspicker_map").addresspicker("map");
                google.maps.event.addListener(map, 'idle', function(){
                    $('#zoom').val(map.getZoom());
                });
            });
        </script>
	<?php wp_head(); ?>
</head>
<?php
global $post;
if(is_page()) {
    $post_slug='page-'.$post->post_name;
} else {
    $post_slug='single-'.$post->post_name;
}
?>
<body <?php body_class( $post_slug.' layout-'.WpvTemplates::get_layout() ); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
	<span id="top"></span>
	<?php do_action( 'wpv_body' ) ?>
	<div id="page" class="main-container">

		<?php include( locate_template( 'templates/header/top.php' ) );?>

		<?php do_action( 'wpv_after_top_header' ) ?>
           
		<div class="boxed-layout">
			<div class="pane-wrapper clearfix">
				<?php include( locate_template( 'templates/header/middle.php' ) );?>
				<div id="main-content">
					<?php include( locate_template( 'templates/header/sub-header.php' ) );?>
					<!-- #main ( do not remove this comment ) -->
					<div id="main" role="main" class="wpv-main layout-<?php echo esc_attr( WpvTemplates::get_layout() ) ?>">
						<?php do_action( 'wpv_inside_main' ) ?>

						<?php if ( WPV_Columns::had_limit_wrapper() ): ?>
							<div class="limit-wrapper">
						<?php endif ?>
