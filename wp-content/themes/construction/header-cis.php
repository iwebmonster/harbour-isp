<?php
/**
 * Header template
 *
 * @package wpv
 * @subpackage construction
 */
?><!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]> <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]> <html <?php language_attributes(); ?> class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?> class="no-ie no-js"> <!--<![endif]-->

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- hello World -->
	<title><?php wp_title(); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php wpvge( 'favicon_url' )?>"/>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        
        <script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
        <link href="<?php echo get_template_directory_uri(); ?>/css/custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

	<?php wp_head(); ?>
</head>
<?php
global $post;
if(is_page()) {
    $post_slug='page-'.$post->post_name;
} else {
    $post_slug='single-'.$post->post_name;
}
?>
<body <?php body_class( $post_slug.' layout-'.WpvTemplates::get_layout() ); ?>>

	<?php do_action( 'wpv_body' ) ?>
	<div id="page" class="main-container">

		<?php do_action( 'wpv_after_top_header' ) ?>

		<div class="">
			<div class="pane-wrapper clearfix">
				<?php include( locate_template( 'templates/header/middle.php' ) );?>
				<div id="main-content">
					<?php include( locate_template( 'templates/header/sub-header-cis.php' ) );?>
					<!-- #main ( do not remove this comment ) -->
					<div id="main" role="main" class="wpv-main layout-<?php echo esc_attr( WpvTemplates::get_layout() ) ?>">
						<?php do_action( 'wpv_inside_main' ) ?>

						<?php if ( WPV_Columns::had_limit_wrapper() ): ?>
							<div class="limit-wrapper">
						<?php endif ?>