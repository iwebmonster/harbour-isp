<?php
/**
 * Single page template
 *
 * Template Name: Custom Sitemap
 *
 * @package wpv
 * @subpackage construction
 */

get_header();
?>


<?php if ( have_posts() ) : the_post(); ?>
	<div class="row page-wrapper">
		<?php WpvTemplates::left_sidebar() ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class( WpvTemplates::get_layout() ); ?>>

                    <div class="sitemap">

                        <?php
if(has_nav_menu( 'custom-sitemap' ))
wp_nav_menu(array(
'theme_location' => 'custom-sitemap',
'walker' => new WpvMenuWalker(),
'link_before' => '<span>',
'link_after' => '</span>',
			));
                        ?>

                    </div>



		<?php WpvTemplates::right_sidebar() ?>

	</div>
<?php endif;

get_footer();

