<?php
/**
 * Header template
 *
 * @package wpv
 * @subpackage construction
 */
?><!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]> <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]> <html <?php language_attributes(); ?> class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?> class="no-ie no-js"> <!--<![endif]-->

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- hello World -->
	<title><?php wp_title(); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php wpvge( 'favicon_url' )?>"/>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        
        <script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
        <?php if( is_page('checkout') ) { ?>
        <script src="<?php echo get_template_directory_uri(); ?>/js/checkout.js"></script>
        <?php } if ( is_product() ) { ?>
        <script src="<?php echo get_template_directory_uri(); ?>/js/single-product.js"></script>
        <?php } ?>
        <link href="<?php echo get_template_directory_uri(); ?>/css/custom.css" rel="stylesheet" type="text/css" media="screen" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        
<!-- Half Circle -->
        <script src="<?php echo get_template_directory_uri(); ?>/js/raphael.2.1.0.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/justgage.1.0.1.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/111.js"></script>
        
<!-- Auto Complete Address -->

        <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.ui.addresspicker.js"></script>
        <script>
            $(document).ready(function(){
                $('#startsearch').click(function(){
                    var fulladdress = $("#addresspicker_map").val();
                    if (fulladdress == '') {
                        $('.search-address div.hello_test').html("Please type in your complete address and try again").show();
                    } else {
                        window.location.href='http://www.nbnco.com.au/connect-home-or-business/check-your-address.html?address='+fulladdress+'&type=home';
                    }
                });
                $('#startsearchbusiness').click(function(){
                    var fulladdress = $("#addresspicker_map").val();
                    if (fulladdress == '') {
                        $('.search-address div.hello_test').html("Please type in your complete address and try again").show();
                    } else {
                        window.location.href='http://www.nbnco.com.au/connect-home-or-business/check-your-address.html?address='+fulladdress+'&type=business';
                    }
                });
                $('#addresspicker_map').click(function(){
                    $('.search-address div.hello_test').hide();
                });
            });
        </script>
        <script>
            $(function() {
                var addresspicker = $( "#addresspicker" ).addresspicker({
                    componentsFilter: 'country:AU'
                });
                var addresspickerMap = $( "#addresspicker_map" ).addresspicker({
                    componentsFilter: 'country:AU',
                    regionBias: "au",
                    language: "au",
                    updateCallback: showCallback,
                    mapOptions: {
                        zoom: 4,
                        center: new google.maps.LatLng(-25.274398, 133.77513599999997),
                        scrollwheel: false,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    },
                    elements: {
                        map: "#map",
                        lat: "#lat",
                        lng: "#lng",
                        street_number: '#street_number',
                        route: '#route',
                        locality: '#locality',
                        sublocality: '#sublocality',
                        administrative_area_level_3: '#administrative_area_level_3',
                        administrative_area_level_2: '#administrative_area_level_2',
                        administrative_area_level_1: '#administrative_area_level_1',
                        country:  '#country',
                        postal_code: '#postal_code',
                        type:    '#type'
                    }
                });
                
                $('#postal_code').keyup(function() {
                    if (this.value == "2560") {
                        $('.datafields').attr("style", "display:block;");
                    }
                    else {
                        $('.datafields').attr("style", "display:none;");
                    }
                });

                var gmarker = addresspickerMap.addresspicker( "marker");
                gmarker.setVisible(true);
                addresspickerMap.addresspicker( "updatePosition");

                $('#reverseGeocode').change(function(){
                    $("#addresspicker_map").addresspicker("option", "reverseGeocode", ($(this).val() === 'true'));
                });

                function showCallback(geocodeResult, parsedGeocodeResult){
                    $('#callback_result').text(JSON.stringify(parsedGeocodeResult, null, 4));
                }
                // Update zoom field
                var map = $("#addresspicker_map").addresspicker("map");
                google.maps.event.addListener(map, 'idle', function(){
                    $('#zoom').val(map.getZoom());
                });
            });
        </script>
<!-- End Autocomplete -->
<!-- FancyBox script -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script> <!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/fancybox/source/jquery.fancybox.js?v=2.1.5"></script><!-- Add fancyBox main JS and CSS files -->
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" /><!-- Add Button helper (this is optional) -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" /><!-- Add Thumbnail helper (this is optional) -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script><!-- Add Media helper (this is optional) -->
<!-- End FancyBox -->
	<?php wp_head(); ?>
</head>
<?php
global $post;
if(is_page()) {
    $post_slug='page-'.$post->post_name;
} else {
    $post_slug='single-'.$post->post_name;
}
?>
<body <?php body_class( $post_slug.' layout-'.WpvTemplates::get_layout() ); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
	<span id="top"></span>
	<?php do_action( 'wpv_body' ) ?>
	<div id="page" class="main-container">

		<?php include( locate_template( 'templates/header/top.php' ) );?>

		<?php do_action( 'wpv_after_top_header' ) ?>

		<div class="boxed-layout">
			<div class="pane-wrapper clearfix">
				<?php include( locate_template( 'templates/header/middle.php' ) );?>
				<div id="main-content">

					<?php include( locate_template( 'templates/header/sub-header.php' ) );?>
					<!-- #main ( do not remove this comment ) -->
					<div id="main" role="main" class="wpv-main layout-<?php echo esc_attr( WpvTemplates::get_layout() ) ?>">
						<?php do_action( 'wpv_inside_main' ) ?>

						<?php if ( WPV_Columns::had_limit_wrapper() ): ?>
							<div class="limit-wrapper">
						<?php endif ?>