<?php

/**
 * Theme functions. Initializes the Vamtam Framework.
 *
 * @package  wpv
 */

require_once( 'vamtam/classes/framework.php' );

new WpvFramework( array(
	'name' => 'construction',
	'slug' => 'construction',
) );

// TODO remove next line when the editor is fully functional, to be packaged as a standalone module with no dependencies to the theme
define( 'VAMTAM_EDITOR_IN_THEME', true ); include_once THEME_DIR . 'vamtam-editor/editor.php';

// only for one page home demos
function wpv_onepage_menu_hrefs( $atts, $item, $args ) {
	if ( 'custom' === $item->type && 0 === strpos( $atts['href'], '/#' ) ) {
		$atts['href'] = $GLOBALS['wpv_inner_path'] . $atts['href'];
	}
	return $atts;
}

if ( ( $path = parse_url( get_home_url(), PHP_URL_PATH ) ) !== null ) {
	$GLOBALS['wpv_inner_path'] = untrailingslashit( $path );
	add_filter( 'nav_menu_link_attributes', 'wpv_onepage_menu_hrefs', 10, 3 );
}

add_filter( 'json_url_prefix', function( $prefix ) { return 're3_'; } );

add_filter('woocommerce_available_variation', function ($value, $object = null, $variation = null) {
    if ($value['price_html'] == '') {
        $value['price_html'] = '<span class="price">' . $variation->get_price_html() . '</span>';
    }
        return $value;
    }, 10, 3); 
    
//REMOVE RELATED POST    
function wc_remove_related_products( $args ) {
	return array();
}
add_filter('woocommerce_related_products_args','wc_remove_related_products', 10); 

/* CUSTOM CODE HARBOUR */


/* Date Picker */
add_filter( 'wp_footer' , 'woo_add_checkout_field_date_range_limit' );
function woo_add_checkout_field_date_range_limit() {
    if ( is_checkout() ) {
        $js = 'jQuery( "#my_birthday" ).datepicker({ defaultDate: "01/01/1970" });';
        //$js = 'jQuery( "#my_birthday" ).datepicker({ minDate: -5, maxDate: "+1M +10D" });'; -- original
        // Check if WC 2.1+
        if ( defined( 'WC_VERSION' ) && WC_VERSION ) {
            wc_enqueue_js( $js );
        } else {
            global $woocommerce;
            $woocommerce->add_inline_js( $js );
        }
    }
}



/**
 * Add checkbox field to the checkout
 **/
add_action('woocommerce_before_order_notes', 'my_custom_checkout_field');
 
function my_custom_checkout_field( $checkout ) {
    
    woocommerce_form_field(
        'my_driverlicense', array(
        'type'          => 'text',
        'class'         => array('form-row-last'),
        'label'         => __('Driver\'s License Number'),
        'required'  => true,
        ), $checkout->get_value( 'my_driverlicense' )
    );
    woocommerce_form_field(
        'my_birthday', array(
        'type'          => 'text',
        'class'         => array('form-row-first'),
        'placeholder'   => __('DD-MM-YYYY'),
        'label'         => __('Date of Birth'),
        'required'  => true,
        ), $checkout->get_value( 'my_birthday' )
    );
    woocommerce_form_field(
        'my_birthday', array(
        'type'          => 'text',
        'class'         => array('form-row-first'),
        'label'         => __('Referrer (name or customer #)'),
        'required'  => false,
        ), $checkout->get_value( 'my_referrer' )
    );
    
 
    echo '<div id="my-new-field"><h3>'.__('Agreement: ').'</h3>';
 
    woocommerce_form_field(
        'my_agree1', array(
        'type'          => 'checkbox',
        'checked'       => 'checked',
        'class'         => array('input-checkbox'),
        'label'         => __(' I’ve read and accept the <a href="#">terms and conditions</a>'),
        'required'  => true,
        ), $checkout->get_value( 'my_agree1' )
    );
    woocommerce_form_field(
        'my_agree2', array(
        'type'          => 'checkbox',
        'class'         => array('input-checkbox'),
        'label'         => __('I have read and understand the Billing & Payment Terms and agree to all charges being deducted via my chosen payment method. I am not a Priority Assist Customer and I am at least 18 years of age'),
        'required'  => true,
        ), $checkout->get_value( 'my_agree2' )
    );
    woocommerce_form_field(
        'my_agree3', array(
        'type'          => 'checkbox',
        'class'         => array('input-checkbox'),
        'label'         => __('I have read and accept the Personal Emergency Response Waiver and understand that my medical alarm device will not function during a power blackout (unless I install a UPS to the ATA and the NBN NTD) or during an internet service interruption. I will not hold Harbour ISP liable for any disruption to my Personal Emergency Response device'),
        'required'  => true,
        ), $checkout->get_value( 'my_agree3' )
    );
    woocommerce_form_field(
        'my_agree4', array(
        'type'          => 'checkbox',
        'class'         => array('input-checkbox'),
        'label'         => __('I have read and accept the CSG Waiver'),
        'required'  => true,
        ), $checkout->get_value( 'my_agree4' )
    );
    echo '</div>';
}
 
/**
 * Process the checkout
 **/
add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');
 
function my_custom_checkout_field_process() {
    global $woocommerce;
 
    // Check if set, if its not set add an error.
    if (!$_POST['my_agree1'])
         wc_add_notice('You must agree on our <strong>Terms and Conditions</strong>', 'error' );
    if (!$_POST['my_agree2'])
         wc_add_notice('You must understand our <strong>Billing and Payment Terms</strong>', 'error' );
    if (!$_POST['my_agree3'])
         wc_add_notice('You must accept our <strong>Personal Emergency Response Waiver</strong>', 'error' );
    if (!$_POST['my_agree4'])
         wc_add_notice('You must read and accept our <strong>CSG Waiver</strong>', 'error' );
    if (!$_POST['my_driverlicense'])
         wc_add_notice('<strong>Driver\'s License Number</strong> is a required field ', 'error' );
    if (!$_POST['my_birthday'])
         wc_add_notice('<strong>Date of Birth</strong> is a required field', 'error' );
}
 
/**
 * Update the order meta with field value
 **/
add_action('woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta');
 
function my_custom_checkout_field_update_order_meta( $order_id ) {
    $_POST['crn1'] = $order_id + 70000;
    if ($_POST['my_agree1']) update_post_meta( $order_id, 'terms-conditions', esc_attr($_POST['my_agree1']));
    if ($_POST['my_agree2']) update_post_meta( $order_id, 'billing-payment-terms', esc_attr($_POST['my_agree2']));
    if ($_POST['my_agree3']) update_post_meta( $order_id, 'personal-emergency-response', esc_attr($_POST['my_agree3']));
    if ($_POST['my_agree4']) update_post_meta( $order_id, 'csg-waiver', esc_attr($_POST['my_agree4']));
    if ($_POST['my_driverlicense']) update_post_meta( $order_id, 'drivers-license', esc_attr($_POST['my_driverlicense']));
    if ($_POST['my_birthday']) update_post_meta( $order_id, 'date-of-birth', esc_attr($_POST['my_birthday']));
    if ($_POST['crn1']) update_post_meta( $order_id, 'crn1', esc_attr($_POST['crn1']));
}


// Remove image from product pages
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
// Remove sale badge from product page
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );


// 1 item on cart
add_filter( 'woocommerce_add_cart_item_data', 'woo_custom_add_to_cart' );

function woo_custom_add_to_cart( $cart_item_data ) {

    global $woocommerce;
    $woocommerce->cart->empty_cart();

    // Do nothing with the data and return
    return $cart_item_data;
}

// redirect to checkout after add to cart
add_filter ('add_to_cart_redirect', 'redirect_to_checkout');

function redirect_to_checkout() {
    global $woocommerce;
    $checkout_url = $woocommerce->cart->get_checkout_url();
    return $checkout_url;
}

// Change the Add to Cart button text

add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );    // 2.1 +
 
function woo_custom_cart_button_text() {
        return __( 'Get this plan', 'woocommerce' );
}



// Display variation's price even if min and max prices are the same
  add_filter('woocommerce_available_variation', function ($value, $object = null, $variation = null) {
    if ($value['price_html'] == '') {
      $value['price_html'] = '<span class="price">' . $variation->get_price_html() . '</span>';
    }
    return $value;
  }, 10, 3);


/* add shortcode on menu settings */
  add_filter('wp_nav_menu_items', 'do_shortcode');

//add_action( 'woocommerce_before_calculate_totals', 'add_custom_price' );

//function add_custom_price( $cart_object ) {
//    $custom_price = $product->id; // This will be your custome price  
//    foreach ( $cart_object->cart_contents as $key => $value ) {
//        $value['data']->price = $custom_price;
//    }
//}
  
  
function so_27023433_disable_checkout_script(){
    wp_dequeue_script( 'wc-checkout' );
}
add_action( 'wp_enqueue_scripts', 'so_27023433_disable_checkout_script' );


// gravity form for products
add_filter( 'woocommerce_gforms_strip_meta_html', 'configure_woocommerce_gforms_strip_meta_html' );
function configure_woocommerce_gforms_strip_meta_html( $strip_html ) {
    $strip_html = false;
    return $strip_html;
}

// disable product reviews
add_filter( 'woocommerce_product_tabs', 'wcs_woo_remove_reviews_tab', 98 );
function wcs_woo_remove_reviews_tab($tabs) {
    unset($tabs['reviews']);
    return $tabs;
}