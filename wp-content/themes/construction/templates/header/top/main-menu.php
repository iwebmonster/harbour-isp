<nav id="main-menu">
	<?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
	<a href="#main" title="<?php esc_attr_e( 'Skip to content', 'construction' ); ?>" class="visuallyhidden"><?php _e( 'Skip to content', 'construction' ); ?></a>
	<?php
        if(is_page(
                array('business-home','business-bundles','business-internet','business-phone','business-hosted-pbx') ) ) {
		if(has_nav_menu( 'top-header-business' ))
			wp_nav_menu(array(
                        'theme_location' => 'top-header-business',
                        'walker' => new WpvMenuWalker(),
                        'link_before' => '<span>',
                        'link_after' => '</span>',
			));
        } elseif (is_page(
                array('my-account','checkout','cart','login') ) ) {
		if(has_nav_menu( 'top-header-account' ))
			wp_nav_menu(array(
                        'theme_location' => 'top-header-account',
                        'walker' => new WpvMenuWalker(),
                        'link_before' => '<span>',
                        'link_after' => '</span>',
			));

        } else {
		if(has_nav_menu( 'top-header' ))
			wp_nav_menu(array(
	   'theme_location' => 'top-header',
	   'walker' => new WpvMenuWalker(),
	   'link_before' => '<span>',
	   'link_after' => '</span>',
			));
        }
	?>
</nav>