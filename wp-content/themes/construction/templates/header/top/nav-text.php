<div class="grid-1-2" id="top-nav-text">
	<?php //echo do_shortcode( wpv_get_option( 'top-bar-text' ) ) // xss ok ?>
    
	<?php
		if(has_nav_menu( 'menu-header' ))
			wp_nav_menu(array(
	   'theme_location' => 'menu-header',
	   'walker' => new WpvMenuWalker(),
	   'link_before' => '<span>',
	   'link_after' => '</span>',
			));
	?>
</div>