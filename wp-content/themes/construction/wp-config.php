<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dishands_wp43');

/** MySQL database username */
define('DB_USER', 'dishands_wp43');

/** MySQL database password */
define('DB_PASSWORD', '6[rP)iJ78S');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'jjsnrg772mndgaowdn9dk3m6upi3idyx141sr8951afeat2fgkgpcz9hso9rf9so');
define('SECURE_AUTH_KEY',  '88airuzxfesvwip5iiphqnfof98puj3dublolmvmsaq6mxzgwdakpehgmoc5y2d2');
define('LOGGED_IN_KEY',    'h1y86nmvvm1gjjhi7cqzwuhhennhdo3gmf0km3qz2wyo0gowjp3suuttvyfotkqo');
define('NONCE_KEY',        '66a26uvlzvdgxabgd7ogpknzd1r7lxqkwymh9yeighvnn6wi049nbk8ylvvnkp1v');
define('AUTH_SALT',        'v1182idiyukzyxxdnohnnhpzjrberpgkiqqcg3vuy5qotcazk1j1cv7a5iugch7w');
define('SECURE_AUTH_SALT', '1vts3aj4q4er50smihxt3kjoxyrs59kpf8s7o63q4sqztptiyvilbdmdzz9ug8gz');
define('LOGGED_IN_SALT',   '5w0huh3mnqrvivyzowrzck9gdey7yhrvqwxwjddvf0pcq3zwi1i6zksg1jlytcc7');
define('NONCE_SALT',       'o0e2p0htuzauxvmqt5hxarpknfnjzxam7lbadoihxkgdkm1kwandfcd5wtbmcbgp');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 're3_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
define( 'WP_MEMORY_LIMIT', '128M' );
define( 'WP_AUTO_UPDATE_CORE', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
