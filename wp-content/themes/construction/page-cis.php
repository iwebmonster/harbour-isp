<?php
/**
 * Single page template
 *
 * Template Name: CIS
 *
 * @package wpv
 * @subpackage construction
 */

get_header(cis);
?>

<?php if ( have_posts() ) : the_post(); ?>
	<div class="row page-wrapper">
		<?php WpvTemplates::left_sidebar() ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class( WpvTemplates::get_layout() ); ?>>
			<?php
			global $wpv_has_header_sidebars;
			if ( $wpv_has_header_sidebars ) {
				WpvTemplates::header_sidebars();
			}
			?>
			<div class="page-content">
				<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'construction' ), 'after' => '</div>' ) ); ?>
				<?php WpvTemplates::share( 'page' ) ?>
                                <?php
                                    $page = get_page_by_title('CIS Footer');
                                    $content = apply_filters('the_content', $page->post_content);
                                    echo $content;
                                ?>
			</div>
			<?php comments_template( '', true ); ?>
		</article>

		<?php WpvTemplates::right_sidebar() ?>

	</div>
<?php endif;

//get_footer(cis);
